# Final Project
# Author: Siyuan Xu
# 2019/12/02

import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
import calendar
def get_data():
    df=pd.read_csv("weather.csv",header=0)
    return df
def np_move_avg(array,n,mode1):#moving average function 
    #n window size 
    #a data 
    #mode= 'same' return the middle values of the convolution as size of a
    return (np.convolve(array,np.ones((n,))/n,mode = mode1))
def main():
#   get data and clear NA
    df=get_data()
    df.fillna(method='ffill',axis=0,inplace=True)

    f=lambda x :32+1.8*x# tansform the tempture to Fahrenheit
    y_min_temp=df['MinTemp']
    y_max_temp=df['MaxTemp']
    y_tmmp9am=df['Temp9am']
    y_tmmp3pm=df['Temp3pm']
    y_temp_mean=( y_min_temp+y_max_temp+y_tmmp9am+y_tmmp3pm)/4
    df['Temp_mavg']=np_move_avg(y_tmmp9am,31,'same')
    y_Wind9=df['WindSpeed9am']
    y_Wind3=df['WindSpeed3pm']
    y_WindGustSpeed=df['WindGustSpeed']
    y_Wind_day_avg=(y_Wind9+y_Wind3)/2
    df['Wind_mov_avg']=np_move_avg( y_Wind_day_avg,31,'same')

 #draw pic1 for temperature   
    fig1=plt.figure(dpi=120)
    plt.style.use('ggplot')
    x1=np.arange(0,366)
    font1={'family':'Times New Roman','weight':'normal','size':14}
    #to set up the font and size for the labels 
 
    plt.title("Daily High and Low Temperature In This year",font1)
    plt.ylabel("Temputure  (c)",font1)
    plt.xlim(xmin=0)
    plt.plot(x1,y_min_temp,"c-",label="Low Temp.")
    plt.plot(x1,y_max_temp,"r-",label="High Temp")
    plt.plot(x1,y_temp_mean,"y-",label="Mean Temp")
    plt.plot(x1,df['Temp_mavg'],"b-",label="Monthly Mean Temp")
    plt.fill_between( x1,y_min_temp,y_max_temp, facecolor='green',label="Diff temp",alpha=0.3)
    plt.xticks([])
    xlocs=range(0,366,30)
    months=calendar.month_name[1:13]  
    plt.xticks(xlocs,months,fontsize =6,color='blue',rotation=60)
    plt.legend(loc='upper right')
    fig1.savefig("DailyTempture",dpi=600)

 #draw pic2  for wind speed  
    fig2=plt.figure(dpi=120)
    x1=np.arange(0,366)
    plt.title("Daily Windspeed In This Year",font1)
    plt.ylabel("Wind Speedy  (mpl)",font1)
    plt.xlim(xmin=0)
    plt.plot(x1,df['Wind_mov_avg'],"b-",label="Monthly Mean Windspeed")
    plt.scatter(x1,y_WindGustSpeed,c='c',marker='*',label="WindGustSpeed")
    plt.xticks([])
    xlocs=range(0,366,30)
    months=calendar.month_name[1:13]
    plt.xticks(xlocs,months,fontsize =6,color='blue',rotation=60)
    plt.legend(loc='upper right',facecolor='yellow')
    fig2.savefig("Windspeed",dpi=600)

 #Draw scatter form wind 9am and 3pm
    fig3=plt.figure(dpi=120)
    x0=y_Wind9
    y0=y_Wind3
    plt.scatter(y_Wind9,y_Wind3,c='c',marker='*',alpha=0.3)
    A=np.polyfit(x0,y0,1)
    y=np.polyval(A,x0)
    plt.plot(x0,y,'b--')
    plt.title("Daily Windspeed Relation Between AM9:00 and PM3:00",font1)
    plt.ylabel("Wind Speedy at pm 3:00  (mpl)",font1)
    plt.xlabel("Wind Speedy at am 9:00  (mpl)",font1)
    fig3.savefig("Windspeed_relation",dpi=600)

  #Draw windguest direction in year
    fig4=plt.figure(dpi=120)
    df1= df['WindGustDir'].groupby( df['WindGustDir']).count()/366*100
    df1=df1[df1.values> 5]
    plt.bar(df1.index,df1.values)
    plt.title('Percent of WindGust Direction in This Year')
    fig4.savefig("PercentWguestDir",dpi=600)
    plt.show()
if __name__ == "__main__":
    main()
