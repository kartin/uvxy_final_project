import csv

with open('UVXY_trans-2019.csv', mode='w') as share_file:
    share_writer = csv.writer(share_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    file = csv.reader(open('UVXY-2019.csv'))

    for line in file:
        line.pop(5)
        
        # swap high and close
        line[2], line[4] = line[4], line[2]
        
        # write into the new csv file
        share_writer.writerow(line)

        
