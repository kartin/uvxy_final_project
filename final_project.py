# Xiaohan Luo, 23317490
# ISTA 131 Final Project, Hayley Anne
# 2019/11/30

from datetime import datetime
from matplotlib import pyplot as plt
from matplotlib import pyplot
import pandas as pd


def first_draw():
    file = pd.read_csv("game.csv", index_col=0, decimal=',')
    title = []
    gamer_score = []
    release_date = []
    for i in file['Title']:
        title.append(i)
    for j in file['Gamer score']:
        gamer_score.append(float(j))
    for z in file['Release date']:
        release_date.append(z)
    xs = [datetime.strptime(d, '%Y/%m/%d').date() for d in release_date]
    plt.plot(xs, gamer_score, "ob")
    plt.xlabel('Time')
    plt.ylabel('Score')
    plt.show()


def second_draw():
    file = pd.read_csv("game.csv", index_col=0, decimal=',')
    total = []
    typs = []
    for j in file['Genre']:
        if j not in typs:
            typs.append(j)
        total.append(j)
    dic = {}
    print(typs)
    for i in total:
        if i not in dic:
            dic[i] = 1
        else:
            dic[i] += 1
    pyplot.pie([dic.get(typ, 0) for typ in typs], labels=typs, autopct='%1.1f%%')
    pyplot.show()


def third_draw():
    file = pd.read_csv("game.csv", index_col=0, decimal=',')
    gamer_score = []
    dic = {}
    for i in file['Gamer score']:
        if i not in gamer_score:
            gamer_score.append(i)
        if i not in dic:
            dic[i] = 1
        else:
            dic[i] += 1
    gamer_score = sorted(gamer_score)
    print(gamer_score)
    print(dic)
    pyplot.bar(range(len(gamer_score)), [dic.get(xtick, 0) for xtick in gamer_score], align='center')
    pyplot.xticks(range(len(gamer_score)), gamer_score)
    pyplot.xlabel("Score")
    pyplot.ylabel("Frequency")
    pyplot.show()


def main():
    first_draw()
    second_draw()
    third_draw()


main()
